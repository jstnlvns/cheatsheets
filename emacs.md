# Emacs Cheatsheet
Here but not in FSNotes?
Blah

* **C = Control**
* **M = Alt (META)**
* **S = Shift**

If letter/key following modifier (control/meta) is capital, this means you have to hold shift.

# File Browsing
| Action | KEYS |
|:--------|------|
| Open file browser | `M-x dired` |




While in dired (file browser)

| Action | KEYS |
|:--------|------|
| Mark all files if no files marked, otherwise marks all non-marked files (can toggle between marked and non-marked) | `t` |
| Unmark all files | `S-U` |
| Mark single file | `m` |
| Search within all marked files using regular expression | `S-A` |
| Open file under cursor in other window | `C-o` |

# Files

|Action|KEYS|
|:-----|----|
|read a file into Emacs| C-x C-f|
|save a file back to disk |C-x C-s|
|save all files| C-x s|
|insert contents of another file into this buffer| C-x i|
|replace this file with the file you really want |C-x C-v|
|write buffer to a specified file |C-x C-w|
|toggle read-only status of buffer |C-x C-q|

# Searching
| Action | KEYS |
|:--------|------|
| Search forward using regular expression for what is under cursor | `C-M-s` then `C-w` press `C-M-s` to go forward `C-M-r` to go backward in document |
| Search forward for what is under cursor | `C-s` then `C-w` press `C-s` to go forward `C-r` to go backward in document |

# Windows
Where `(N)`, action creates new window.

| Action | KEYS |
|:--------|------|
| Close window where cursor is | `C-0` |
| Keep window where cursor is, but close all others | `C-1` |
| **(N)** Split window - horizontal (above-and-below) | `C-2` |
| **(N)** Split window - vertical (side-by-side) | `C-3` |
| Switch cursor to other window | `C-o` |
| **(N)** Open `dired` in other window | `C-x 4 d` |
| **(N)** Find/Open New File in other window | `C-x 4 f` |
| **(N)** Select buffer in other window | `C-x 4 b` |

# Transposing
| Action | KEYS |
|:--------|------|
| Transpose lines | `C-x C-t` |
| Transpose words | `M-t` |

# DIRED
These actions can be used in DIRED or in buffer list.

| Action | KEYS |
|:--------|-----|
|start up dired|`C-x d`|
|copy|`C`|
|mark for erase|`d`|
|delete right away|`D`|
|open file or directory|`e` or `f`|
|reread directory structure from file|`g`|
|change group permissions (chgrp)|`G`|
|delete line from listing on screen (don't actually delete)|`k`|
|mark with *|`m`|
|move to next line|`n`|
|open file in other window and go there|`o`|
|open file in other window but don't change there|`C-o`|
|print file|`P`|
|quit dired|`q`|
|do query-replace in marked files|`Q`|
|rename file|`R`|
|remove mark|`u`|
|view file content|`v`|
|delete files marked with D|`x`|
|compress file|`z`|
|remove all marks (whatever kind)|`M-Del`|
|mark backup files (name~ files) for deletion|`~`|
|mark auto-save files (name) for deletion|`#`|
|mark directory with * (C-u * removes that mark again)|`*/`|
|compare this file with marked file|`=`|
|compare this file with it's backup file|`M-=`|
|apply shell command to this file|`!`|
|change to the next file marked with * od D|`M-}`|
|change to the previous file marked with * od D"|`M-{`|
|mark files described through regular expression for deletion|`% d`|
|mark with *|`% m`|
|create directory|`+`|
|changed to next dir|`>`|
|change to previous dir|`<`|
|toggle between sorting by name or date|`s`|

# Marking

|Action|KEYS|
|:-----|----|
|set mark here| C-@ or C-SPC|
|exchange point and mark| C-x C-x|
|set mark arg words away| M-@|
|mark paragraph| M-h|
|mark page |C-x C-p|
|mark sexp| C-M-@|
|mark function |C-M-h|
|mark entire buffer| C-x h|

# Killing and Deleting

|entity to kill| backward |forward|
|--------------|------|-----------|
character (delete, not kill)| DEL| C-d|
|word| M-DEL| M-d|
|line (to end of)| M-0| C-k C-k|
|sentence| C-x DEL| M-k|
|sexp| M-- C-M-k |C-M-k|

|Other Situation|KEYS|
|---------------|----|
|kill region |C-w|
|copy region to kill ring| M-w|
|kill through next occurrence of char| M-z char|
|yank back last thing killed |C-y|
|replace last yank with previous kill| M-y|


# Formatting

|Action|KEYS|
|:-----|----|
|indent current line (mode-dependent)| TAB|
|indent region (mode-dependent) |C-M-\
|indent sexp (mode-dependent) |C-M-q|
|indent region rigidly arg columns |C-x TAB|
|indent for comment| M-;|
|insert newline after point| C-o|
|move rest of line vertically down| C-M-o|
|delete blank lines around point| C-x C-o|
|join line with previous (with arg, next)| M-^|
|delete all white space around point |M-\|
|put exactly one space at point |M-SPC|
|fill paragraph| M-q|
|set fill column to arg |C-x f|
|set prefix each line starts with |C-x .|
|set face |M-o|

# Case Change

|Action|KEYS|
|:-----|----|
|uppercase word| M-u|
|lowercase word| M-l|
|capitalize word |M-c|
|uppercase region| C-x C-u|
|lowercase region |C-x C-l|

# The Minibuffer

The following keys are defined in the minibuffer.

|Action|KEYS|
|:-----|----|
|complete as much as possible| TAB|
|complete up to one word |SPC|
|complete and execute |RET|
|show possible completions |?|
|fetch previous minibuffer input| M-p|
|fetch later minibuffer input or default| M-n|
|regexp search backward through history| M-r|
|regexp search forward through history |M-s|
|abort command |C-g|

Type C-x ESC ESC to edit and repeat the last command that
used the minibuffer. Type F10 to activate menu bar items on
text terminals.


# Keyboard Macros

|Action|KEYS|
|:-----|----|
|start defining a keyboard macro| C-x (|
|end keyboard macro definition |C-x )|
|execute last-defined keyboard macro |C-x e|
|append to last keyboard macro |C-u C-x (|
|name last keyboard macro |M-x name-last-kbd-macro|
|insert Lisp definition in buffer| M-x insert-kbd-macro|
